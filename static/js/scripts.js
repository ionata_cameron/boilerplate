jQuery(function($) {

	// Placeholders
	(function(p,n){
		if(Modernizr.input[p])return;
		$('['+p+']').each(function(z,i){
			var h=$(i).attr(p);
			$(i).focus(function(){
				if($(i).val()==h) $(i).val(n).removeClass(p);
			}).blur(function(){
				if($(i).val()==n||$(i).val()==h) $(i).addClass(p).val(h);
			}).blur().parents('form').submit(function(){
				$(this).find('['+p+']').each(function(z,f){
					if($(f).val()=$(f).attr(p)) $(f).val(n);
				});
			});
		});
	})('placeholder','');
	
});

